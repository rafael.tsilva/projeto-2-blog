import React from 'react'
import './Busca.css'

const Busca = () => {
    return (
        <div id='busca'>
            <input id='b-campo-busca' placeholder='Pesquisar' title='Pesquisar'></input>
            <button id='b-btn-busca'><img src='/img/baixados.png' alt='Imagem de uma lupa representando uma busca' />  </button>
        </div>
    );
}

export default Busca;