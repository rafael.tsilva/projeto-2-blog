import React from 'react'
import './ListaCategorias.css'
import { Link } from 'react-router-dom';

const ListaCategorias = ({lista}) => {
    return (
        <div id='bl-lista-categorias'>
            
            <h4>Categorias</h4>
            <ul>
            {lista ? lista.map(item =>
            <li key={item.id}>
                            <Link to={`/posts-por-categoria/${item.id}`}>{item.descricao}</Link>
                         </li>
                ) : <p>Carregando</p>
            }
               </ul>

        </div>
    );
}

export default ListaCategorias;