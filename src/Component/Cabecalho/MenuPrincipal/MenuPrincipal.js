import React, {useContext} from 'react'
import './MenuPrincipal.css'
import { Link } from 'react-router-dom';
import ContextTheme from '../../../contexts/ContextTheme';

const MenuPrincipal = () => {

     const theme = useContext(ContextTheme)

     
        
    return (
       <nav style={{backgroundColor: theme.corFundoTema}}>

           <ul>
               <li>
                   <Link to='/lista-post'>Post</Link>
               </li>
               <li>
                    <Link to='/lista-categoria'>Categoria</Link>
               </li>
               <li>
                    <Link to='/novo-post'>Novo Post</Link>
               </li>
           </ul>
       </nav>
    );
}

export default MenuPrincipal;