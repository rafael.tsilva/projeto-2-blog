import React, {useContext} from 'react'
import './Cabecalho.css'
import ContextTheme from '../../../src/contexts/ContextTheme';
import MenuPrincipal from './MenuPrincipal/MenuPrincipal'

import {COR_1, COR_2, COR_3, COR_4, COR_PADRAO} from '../../utils/coresTemas';


const Cabecalho = ({functionThemeConfig}) => {
    const theme = useContext(ContextTheme)
    return (
        <header id='box-cabecalho' style={{backgroundColor: theme.corFundoTema}}>
               <div id='c-temas'>
                    
                    <button role='button' id='c-tema-branco' onClick={ () => {functionThemeConfig(COR_1)}}>white</button>
                    <button role='button' id='c-tema-preto'onClick={ () => {functionThemeConfig(COR_2)}}>preto</button>
                    <button role='button' id='c-tema-amarelo'onClick={ () => {functionThemeConfig(COR_3)}}>amarelo</button>
                    <button role='button' id='c-tema-azul'onClick={ () => {functionThemeConfig(COR_4)}}>azul</button>
                    <button role='button' id='c-tema-default 'onClick={ () => {functionThemeConfig(COR_PADRAO)}}>default</button>
                </div>
                
            <div id='blog-temas' style={{backgroundColor: theme.corFundoTema}}>
                <h1 >Blog</h1> <MenuPrincipal />
                
               
            </div>
         
            
            
        </header>
    );
}

export default Cabecalho;