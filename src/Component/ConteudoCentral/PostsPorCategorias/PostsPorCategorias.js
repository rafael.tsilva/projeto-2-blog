import React, { useEffect, useState } from 'react'
import './PostsPorCategorias.css'
import { useParams } from 'react-router';
import pegarPosts from '../../../utils/pegarPosts';

const PostsPorCategorias = () => {

    const {idCategoria} = useParams();

    const [posts, setPosts] = useState([]);
    const [postsFiltrados, setPostsFiltrados] = useState([]);

   
    
    useEffect(()=>{
        pegarPosts(setPosts);
    },[]);

    useEffect(()=>{ 
        if(posts.length > 0){
        const _postsFiltrados = posts.filter(posts => posts.idCategoria === parseInt(idCategoria) );
        setPostsFiltrados( _postsFiltrados )
        }
    },[posts, idCategoria]);
    
    return (
        <div id='cc-posts-por-categorias'>
            <h2>Posts Por Categorias</h2>
            <p>id selecionado: {idCategoria}</p>
        <div>
            {postsFiltrados.map( post => { return <p>{post.id} - {post.idCategoria} - {post.descricao} </p>})}
        </div>

        </div>
        
    );
}

export default PostsPorCategorias;