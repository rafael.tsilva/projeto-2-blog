import React, {useEffect, useState}  from 'react'
import { Route, Switch, Redirect} from 'react-router-dom';

import './ConteudoCentral.css'

import ListaPost from './ListaPost/ListaPost'
import ListaCategorias from './ListaCategorias/ListaCategorias'
import NovoPost from './NovoPost/NovoPost'
import pegarCategorias from '../../utils/pegarCategorias';
import DetalhesPost from './DetalhesPost/DetalhesPost';
import PostsPorCategorias from './PostsPorCategorias/PostsPorCategorias';


const ConteudoCentral = () => {

    const [categorias, setCategorias] = useState([])
    
 

    useEffect(() => {
        pegarCategorias (setCategorias);
    }, []);

    

   

    return (
        <main id='box-conteudoCentral'>
            <Switch>
                <Route exact path='/'><Redirect to='/lista-post' /></Route>
                <Route exact path='/lista-post'><ListaPost /></Route>
                <Route path='/lista-categoria'><ListaCategorias lista={categorias}/></Route>
                <Route path='/novo-post'><NovoPost /></Route>
                <Route path='/detalhes-post/:id' component={DetalhesPost}></Route>
                <Route path='/posts-por-categoria/:idCategoria' component={PostsPorCategorias}></Route>

            </Switch>

        </main>
    );
}

export default ConteudoCentral;