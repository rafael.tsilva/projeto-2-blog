import React from 'react'
import './ListaCategorias.css'
import { Link } from 'react-router-dom';

const ListaCategorias = ({lista}) => {

  
    return (
        <div id='lista-categorias'>
            <div id='lc-box-btn'>

            <h2 id='lc-titulo'>Lista Categorias</h2>            
            <button id='lc-btn-nova-categoria'> Nova Categoria</button>

            </div>

            <ul id='lc-categorias'>
                {lista ? lista.map(item => 
                 <li key={item.id} className='lc-item'>
                        {item.descricao}
                        <button id='lc-btn-remover'>Remover</button>
                        <Link id='lc-btn-listar-posts' to={`/posts-por-categoria/${item.id}`}>Listar Posts</Link>
                    </li>
                )
            : <p>Carregando Categorias</p>
            }
                
            </ul>
        </div>
    );
}

export default ListaCategorias;