import React, {useContext} from 'react'
import './Post.css'
import ContextTheme from '../../../../contexts/ContextTheme';
import { Link } from 'react-router-dom';

const Post = ({post}) => {

    const theme = useContext(ContextTheme)

    return (
        <article className='post' style={{backgroundColor: theme.corFundoTema}}>

            <h3 className='p-titulo'>{post.nome}</h3>
            {/* <p className='p-postado-em'>{post.resumo}</p> */}

            <div className='p-img-texto'>
                <div className='p-img'>
                <img src={post.imagem} />
                </div>
                <div className='p-texto'>
                    {post.resumo}
                </div>
            </div >

                <div className='p-categorias'>
                    <div >
                        {post.idCategoria}
                    </div>
                    
                        <Link to={`/detalhes-post/${post.id}`} className='p-btn-continue-lendo' style={
                            {background: theme.corFundoBotaoContinueLendo, 
                            color: theme.corTextoBotaoContinueLendo}}>Continue lendo</Link>
                    </div>
                    
        </article>
       
    );
}

export default Post;