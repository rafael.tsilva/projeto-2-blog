import React from 'react'
import './ListaPost.css'
import { useEffect, useState } from 'react'
import axios from 'axios'
import Post from './Post/Post'

const ListaPost = () => {

    const [posts, setPosts] = useState([])

    const pegarPost = async () => {
        const lista = await axios.get('http://localhost:3001/v1/artigo')
        const result = Object.values(lista.data.artigos)
                
       setPosts(result)
    }
    
    // [lista.data].map(item => item.artigos)
    useEffect( () => {
        pegarPost();
        
    }, []);

      
    return (
        <div>

            {/* {posts.artigos.map(item => console.log("segundo"+item))} */}
            {posts.length > 0 ?
            posts.map(item => <Post post={item}/>)
             : <p>Carregando posts post</p>}
                    <div className='card'>
                    <div className='card-content'>
                        <h2 className='card-title'>Hunter x Hunter</h2>
                        <p className='card-body'>É uma série de mangá escrita e ilustrada por Yoshihiro Togashi. Serializados na revista Weekly Shōnen Jump desde 3 de março de 1998, embora o mangá tenha frequentemente entrado em hiato desde 2006.</p>
                        <a href='#' className='button'>Saiba Mais</a>
                    </div>
                    </div>
        </div>

    );
}

export default ListaPost;