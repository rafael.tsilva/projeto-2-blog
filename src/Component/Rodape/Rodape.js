import React from 'react'
import './Rodape.css'

const Rodape = () => {
    return (
        <footer id='box-rodape'>
            Copyright &#169; - TelesCorporation
        </footer>
    );
}

export default Rodape;