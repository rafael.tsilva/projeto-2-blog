import React, { useState } from 'react';
import { BrowserRouter as Router} from 'react-router-dom';


import './Home.css'

import Cabecalho from '../Cabecalho/Cabecalho'
import ConteudoCentral from '../ConteudoCentral/ConteudoCentral'
import BarraLateral from '../BarraLateral/BarraLateral'
import Rodape from '../Rodape/Rodape'

import ContextTheme from '../../contexts/ContextTheme'

import {COR_1, COR_2, COR_3, COR_4} from '../../utils/coresTemas';
import {COR_1_CONFIG, COR_2_CONFIG, COR_3_CONFIG, COR_4_CONFIG, COR_PADRAO_CONFIG} from '../../utils/coresTemas';

const Home = () => {
        
    const [theme, setTheme] = useState(COR_PADRAO_CONFIG)

    const modifyTheme = selectedTheme => {
        // console.log(`selectedTheme: ${selectedTheme}`);
        switch (selectedTheme) {
            case COR_1:
                setTheme(COR_1_CONFIG);
                break;
            case COR_2:
                setTheme(COR_2_CONFIG);
                break;
            case COR_3:
                setTheme(COR_3_CONFIG);
                break;
            case COR_4:
                setTheme(COR_4_CONFIG);
                break;
        
            default:
                setTheme( COR_PADRAO_CONFIG );
                break;
        }
    }

    return (
        <ContextTheme.Provider value={theme}>
        <Router>

            <div id='box-home'>

                <Cabecalho functionThemeConfig={modifyTheme}/>

                <div id='container'>

                    <ConteudoCentral />
                    <BarraLateral />

                </div>

                <Rodape />

            </div>

        </Router>
        </ContextTheme.Provider>
    );
}

export default Home