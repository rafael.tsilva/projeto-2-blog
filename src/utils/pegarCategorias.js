import axios from "../axios/axios";

const pegarCategorias =  async (configState) => {

    //Mock 

    // const _listaCategorias = [
    //     {
    //         'id': 1,
    //         'descricao': 'Montanha'
    //     },
    //     {
    //         'id': 2,
    //         'descricao': 'Planície'
    //     },
    //     {
    //         'id': 3,
    //         'descricao': 'Ilha'
    //     },
    //     {
    //         'id': 4,
    //         'descricao': 'Pântano'
    //     }
    // ];

    try {
        const resposta = await axios.get('categoria');
        const _listaCategorias = resposta.data.categorias;


        _listaCategorias.sort((a,b)=> {
            return (a.categoria > b.categoria) ? 1 : -1;
    });
    configState(_listaCategorias);

    }catch (erro){
        console.log(`Ocorreu um erro: ${erro.message}.`)
    }

    
    

    
};

export default pegarCategorias;