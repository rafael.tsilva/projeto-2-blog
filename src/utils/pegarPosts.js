import axios from "../axios/axios";

const pegarPosts = async (salvarState) => {

    //Mock 

    // const _listaPosts = [
    //     {
    //         'id': 1,
    //         'idCategoria': 2,
    //         'descricao': 'post 1'
    //     },
    //     {
    //         'id': 2,
    //         'idCategoria': 2,
    //         'descricao': 'Post 2'
    //     },
    //     {
    //         'id': 3,
    //         'idCategoria': 1,
    //         'descricao': 'post 3'
    //     },
    //     {
    //         'id': 4,
    //         'idCategoria': 1,
    //         'descricao': 'Post 4'
    //     }
    // ];

    try {
        const resposta = await axios.get('artigo')
        salvarState(resposta.data);
        // console.log(resposta.data);
    } catch (error) {
        console.log(`Ocorreu um erro: ${error.message} no pegar post.`)
    }
    
    
};

export default pegarPosts;