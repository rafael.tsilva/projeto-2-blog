const COR_PADRAO = 'default';
const COR_1 = 'white';
const COR_2 = 'black';
const COR_3 = 'yellow';
const COR_4 = 'blue';

const COR_PADRAO_CONFIG = {
    corFundoTema: 'white',
    corFundoBotaoContinueLendo: 'black',
    corTextoBotaoContinueLendo: 'white'
};

const COR_1_CONFIG = {
    corFundoTema: 'lightgray',
    corFundoBotaoContinueLendo: 'gray',
    corTextoBotaoContinueLendo: 'white'
};

const COR_2_CONFIG = {
    corFundoTema: 'black',
    corFundoBotaoContinueLendo: 'black',
    corTextoBotaoContinueLendo: 'white'
};

const COR_3_CONFIG = {
    corFundoTema: 'lightyellow',
    corFundoBotaoContinueLendo: 'yellow',
    corTextoBotaoContinueLendo: 'black'
};

const COR_4_CONFIG = {
    corFundoTema: 'lightblue',
    corFundoBotaoContinueLendo: 'blue',
    corTextoBotaoContinueLendo: 'white'
};


export {COR_1, COR_2, COR_3, COR_4, COR_PADRAO};
export {COR_1_CONFIG, COR_2_CONFIG, COR_3_CONFIG, COR_4_CONFIG, COR_PADRAO_CONFIG}